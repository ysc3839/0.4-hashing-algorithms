#include "SQImports.h"
#include "SQMain.h"
#include "SQFuncs.h"
#include <stdio.h>

PluginFuncs * gFuncs;
HSQUIRRELVM v;
HSQAPI sq;

void OnSquirrelScriptLoad()
{
	// See if we have any imports from Squirrel
	size_t size;
	int     sqId      = gFuncs->FindPlugin( "SQHost2" );
	const void ** sqExports = gFuncs->GetPluginExports( sqId, &size );

	// We do!
	if( sqExports != NULL && size > 0 )
	{
		// Cast to a SquirrelImports structure
		SquirrelImports ** sqDerefFuncs = (SquirrelImports **)sqExports;
		
		// Now let's change that to a SquirrelImports pointer
		SquirrelImports * sqFuncs       = (SquirrelImports *)(*sqDerefFuncs);
		
		// Now we get the virtual machine
		if( sqFuncs )
		{
			// Get a pointer to the VM and API
			sq = *(sqFuncs->GetSquirrelAPI());
			v = *(sqFuncs->GetSquirrelVM());

			// Register functions
			RegisterFuncs( v );
		}
	}
	else
		printf( "Failed to attach to SQHost2.\n" );
}

uint8_t OnInternalCommand(unsigned int uCmdType, const char* pszText)
{
	switch( uCmdType )
	{
		case 0x7D6E22D8:
			OnSquirrelScriptLoad();
			break;

		default:
			break;
	}

	return 1;
}

extern "C" EXPORT unsigned int VcmpPluginInit( PluginFuncs* functions, PluginCallbacks* callbacks, PluginInfo* info )
{
    info->apiMajorVersion = PLUGIN_API_MAJOR;
    info->apiMinorVersion = PLUGIN_API_MINOR;

	gFuncs = functions;
	callbacks->OnPluginCommand = OnInternalCommand;
	return 1;
}
